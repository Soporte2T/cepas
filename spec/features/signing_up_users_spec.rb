require "rails_helper"

RSpec.feature "Singup Bros" do
    
    scenario "with valid credentials" do
    visit"/"
    click_link "Sign up"
    fill_in "Email", with: "u@e.com"
    fill_in "Password",  with:"asdfghjkl"
    fill_in "Password confirmation",  with:"asdfghjkl"
    click_link "Sign up"
    expect(page).to have_content("Welcome! You have signed up successfully.")
    end

    scenario "with invalid credentials" do
    visit"/"
    click_link "Sign up"
    fill_in "Email", with: ""
    fill_in "Password",  with:""
    fill_in "Password confirmation",  with:""
    click_link "Sign up"
    expect(page).to have_content("Email can't be blank")
    
    end    

end