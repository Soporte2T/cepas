Rails.application.routes.draw do
  devise_for :bros
  root to: "articles#index"
  
  resources :articles
end
